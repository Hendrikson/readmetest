# Mercurial vs Git

|                           Description                           |      Mercurial     |         Git         |
| :-------------------------------------------------------------- | :----------------- | :------------------ |
| Start new Repository                                            | hg init            | git init            |
| Clone Repository                                                | hg clone           | git clone           |
| Add file to staging area                                        | hg add             | git add             |
| Commit files to version history                                 | hg commit          | git commit          |
| Commit with message                                             | hg commit -m ""    | git commit -m ""    |
| Sends commited changes to remote repository                     | hg push            | git push            |
| Fetches and merges changes from remote repository               | hg pull -update    | git pull            |
| Merge specific branch history into current branch               | hg merge           | git merge           |
|                                                                 |                    |                     |
| List version history                                            | hg log             | git log             |
| Shows file differences, that are not yet staged                 | hg diff            | git diff            |
| Show what revision and author last modified each line of a file | hg blame           | git blame           |
| Shows metadata of specific commit                               | hg cat <commit>    | git show <commit>   |
| Lists all files that have to be commited                        | hg status          | git status          |
| Move or rename tracked file                                     | hg move/rename     | git mv              |
| Remove file from directory & project staging area               | hg remove          | git rm              |
| Cancel uncommited merge                                         | hg revert          | git checkout -f     |
|                                                                 |                    |                     |
| Display branches                                                | hg bookmarks       | git branch          |
| Switch to another branch                                        | hg bookmark <name> | git checkout <name> |


# Gitflow-avh


|                  Gitflow-avh               |                                                                                Git                                                                                                           |
|:------------------------------------------ |:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `$ git flow init`                          | `$ git init`<br>`$ git commit --allow-empty -m "Initial commit"`<br>`$ git checkout -b develop master`                                                                                            |
| `$ git flow feature start MYFEATURE`       | `$ git checkout -b feature/MYFEATURE develop`                                                                                                                                                |
| `$ git flow feature finish MYFEATURE`      | `$ git checkout develop`<br>`$ git merge --no-ff feature/MYFEATURE`<br>`$ git branch -d feature/MYFEATURE`                                                                                   |
| `$ git flow feature publish MYFEATURE`     | `$ git checkout feature/MYFEATURE`<br>`$ git push origin feature/MYFEATURE`                                                                                                                  |
| `$ git flow feature pull origin MYFEATURE` | `$ git checkout feature/MYFEATURE`<br>`$ git pull --rebase origin feature/MYFEATURE`                                                                                 |
|                                            |                                                                                                                          |
| `$ git flow release start 1.2.0`           | `$ git checkout -b release/1.2.0 develop`                                                                                                                                                    |
| `$ git flow release finish 1.2.0`          | `$ git checkout master`<br>`$ git merge --no-ff release/1.2.0`<br>`$ git tag -a 1.2.0`<br>`$ git checkout develop`<br>`$ git merge --no-ff release/1.2.0`<br>`$ git branch -d release/1.2.0` |
| `$ git flow release publish 1.2.0`         | `$ git checkout release/1.2.0`<br>`$ git push origin release/1.2.0`                                                                                                                          |
|                                            |                                                                                                                  |
| `$ git flow hotfix start 1.2.1 <commit>`   | `$ git checkout -b hotfix/1.2.1 <commit>`                                                                                                                                                    |
| `$ git flow hotfix finish 1.2.0`           | `$ git checkout master`<br>`$ git merge --no-ff hotfix/1.2.1`<br>`$ git tag -a 1.2.1`<br>`$ git checkout develop`<br>`$ git merge --no-ff hotfix/1.2.1`<br>`$ git branch -d hotfix/1.2.1`    |